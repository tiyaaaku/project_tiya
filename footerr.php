<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versi</b> 1.0
    </div>
    <strong>Hak Cipta &copy; <?= date('Y'); ?>. Tiya</strong> Siti Asratul R. <a href='https://stokcoding.com/' title='StokCoding.com' target='_blank'>StokCoding.com</a>
    
</footer>